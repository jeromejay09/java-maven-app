<<<<<<< HEAD
FROM amazoncorretto:8-alpine3.17-jre

EXPOSE 8080

COPY ./target/java-maven-app-*.jar /usr/app/
WORKDIR /usr/app

CMD java -jar java-maven-app-*.jar
=======
# Step 1: Use a Maven image to build the application
FROM maven:3.8.5-openjdk-17 AS build

# Set the working directory in the container
WORKDIR /app

# Copy the entire project into the container
COPY . .

# Use Maven to compile and package the application
RUN mvn clean package

# Step 2: Use a lightweight JDK image to run the application
FROM openjdk:17-jdk-slim

# Set the working directory for the runtime container
WORKDIR /app

# Copy the built JAR file from the build stage
COPY --from=build /app/target/HelloJeromeApp-1.0-SNAPSHOT.jar /app/HelloJeromeApp.jar

# Expose the application port
EXPOSE 3000

# Define the command to run the application
CMD ["java", "-jar", "HelloJeromeApp.jar"]
>>>>>>> 1be4bc0 (Initial commit)
