package com.example;

import static spark.Spark.*;

public class HelloJeromeApp {
    public static void main(String[] args) {
        // Set the port to 3000
        port(3000);

        // Define a route for the root URL ("/")
        get("/", (req, res) -> "Hello Jerome, Nestlee and Jazmine!");
    }
}
